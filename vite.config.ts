import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    rollupOptions: {
      output: {
        // 指定输出目录
        dir: 'dist',
        // 指定输出文件的名称
        entryFileNames: 'bundle.js'
      },
    }
  }
})
