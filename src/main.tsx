'use client'
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import Web3ModalProvider from './context/web3ModalProvider'

const renderAppContainer = () => {
  const body = document.getElementsByTagName('body')[0];
  const div = Object.assign(document.createElement('div'), {
    id: "root",
  });
  body.appendChild(div);
  ReactDOM.createRoot(div).render(
    <React.StrictMode>
      <Web3ModalProvider>
        <App />
      </Web3ModalProvider>
    </React.StrictMode>,
  )
}

const renderAndInit = () => {
  renderAppContainer()
  if (!isRendered()) {
    console.error("React not rendered, returning")
    return
  }
}

const isRendered = () => {
  return !!document.getElementById("root")
}

document.addEventListener("DOMContentLoaded", () => {
  renderAndInit()
});

window.onload = () => {
  if (!isRendered()) {
    console.warn("React re-rendering in window.onload")
    renderAndInit()
  }
}