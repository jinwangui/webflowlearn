import { useEffect } from 'react'
import './App.css'
import { useWeb3Modal } from '@web3modal/wagmi/react'
import { useAccount, useSignMessage } from 'wagmi';

function App() {
  const {open} = useWeb3Modal()
  const {
    signMessage,
    data: signature,
    isPending: isSignPending,
    error: signError
  } = useSignMessage()
  const {chainId, address, isConnecting, isConnected} = useAccount();

  useEffect(()=>{
    const dom = document.getElementById('connect')
    if(!dom) return
    dom.addEventListener('click',()=>{
        open()
    })
  },[])

  console.log(signature,isSignPending,signError,signMessage)
  console.log(chainId, address, isConnecting, isConnected)

  return (
    <></>
  )
}

export default App
