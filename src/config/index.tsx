import { defaultWagmiConfig } from '@web3modal/wagmi/react/config'

import { cookieStorage, createStorage } from 'wagmi'
import { base, baseSepolia  } from 'wagmi/chains'

// Get projectId at https://cloud.walletconnect.com
// export const projectId = process.env.NEXT_PUBLIC_WALLET_CONNECT_PROJECT_ID;
export const projectId = '6c751343963f5357d7546a53fb45f477'

if (!projectId) throw new Error('Project ID is not defined')

const metadata = {
  name: 'Web3Modal',
  description: 'Web3Modal Example',
  url: 'https://web3modal.com', // origin must match your domain & subdomain
  icons: ['https://avatars.githubusercontent.com/u/37784886']
}
export const CHAIN = {
  dev: baseSepolia,
  beta: baseSepolia,
  pre: base,
  prod: base
}[process.env.NEXT_PUBLIC_ENV || 'dev'] || baseSepolia
// Create wagmiConfig
const chains = [CHAIN] as const
export const config = defaultWagmiConfig({
  chains,
  projectId,
  metadata,
  ssr: true,
  storage: createStorage({
    storage: cookieStorage
  }),
})